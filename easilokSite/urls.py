"""easilokSite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from markdownx import urls as markdownx

admin.autodiscover()

urlpatterns = [ 
        path('lord/', admin.site.urls),
        path('api/', include('portfolio_api.urls'))
]

if 'markdownx' in settings.INSTALLED_APPS:
    urlpatterns += [
        re_path("^markdownx/", include(markdownx))
    ]


urlpatterns += i18n_patterns(
    path("", include("cms.urls"))
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


# urlpatterns += i18n_patterns(
#         path('', include('homepage.urls')),
#         path('blog/', include('blog.urls')),
#         path('curriculum/', include('curriculum.urls')),
#         path('portfolio/', include('portfolio.urls')),
#         )

# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + \
#                 static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

