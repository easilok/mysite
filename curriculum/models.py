from django.db import models

class Curriculum(models.Model):
    name = models.CharField(max_length=100, unique=True)
    path = models.CharField(max_length=100)
    filename = models.CharField(max_length=100, default="")
    order = models.IntegerField(default=0)
    downloads = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    CURRICULUM_LANGUAGE = (
        (0, 'en'),
        (1, 'pt'),
    )
    language = models.IntegerField(choices=CURRICULUM_LANGUAGE, default=0)

    class Meta:
        ordering = ['order', 'name']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return "/curriculum/" + str(self.id)
