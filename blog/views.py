from django.shortcuts import render, get_object_or_404
from blog.models import Category, Tag, Post, BlogImage
from django.views import generic
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import BlogImageForm

class PostListView(generic.ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'blog.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            posts = Post.objects.all()
            pinned = Post.objects.filter(pinned = True).order_by('pinnedOrder')
        else:
            posts = Post.objects.filter(public = True)
            pinned = Post.objects.filter(public = True).filter(pinned = True).order_by('pinnedOrder')

        for post in pinned:
            post.tags = post.tag.all()
            post.categories = post.category.all()

        for post in posts:
            post.tags = post.tag.all()
            post.categories = post.category.all()

        context['pinned'] = pinned
        context['posts'] = posts
        context['navMenu'] = 'blog'

        return context

def blogShow(request, blog_id):
    post = get_object_or_404(Post, pk=blog_id)
    if not post.public:
        if not request.user.is_authenticated:
             raise Http404("Post does not exist")

    post.tags = post.tag.all()
    post.categories = post.category.all()

    context = {
        'post': post,
        'navMenu': 'blog'
    }

    return render(request, 'blog_detail.html', context=context)

class BlogImageUploadView(LoginRequiredMixin, generic.FormView):
    raise_exception = True
    template_name = 'blog_image_upload.html'
    form_class = BlogImageForm

    def form_valid(self, form):
        blog_image = BlogImage(
            image=self.get_form_kwargs().get('files')['image'])
        blog_image.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('blog')

class BlogImageListView(LoginRequiredMixin, generic.ListView):
    raise_exception = True
    model = BlogImage
    context_object_name = 'images'
    template_name = 'blog_image_list.html'
    paginate_by = 10
