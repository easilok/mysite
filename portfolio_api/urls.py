from django.urls import path, include
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
# router.register(r'projects/{projectId}', views.ProjectViewSet)
router.register(r'portfolio', views.ProjectListSet)

urlpatterns = [
        path('', include(router.urls))
]
