from django import forms

class BlogImageForm(forms.Form):
    image = forms.FileField(label="Select image to import")
