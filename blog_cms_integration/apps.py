from django.apps import AppConfig


class BlogCmsIntegrationConfig(AppConfig):
    name = 'blog_cms_integration'
