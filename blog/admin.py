from django.contrib import admin
from .models import Author, Language, Category, Tag, Post, BlogImage

class PostAdmin(admin.ModelAdmin):
    exclude = ('contentHTML',)

# Register your models here.
admin.site.register(Language)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Author)
admin.site.register(Post, PostAdmin)
admin.site.register(BlogImage)
