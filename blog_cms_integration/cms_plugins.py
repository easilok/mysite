from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from blog_cms_integration.models import BlogPluginModel
from django.utils.translation import ugettext as _


@plugin_pool.register_plugin  # register the plugin
class BlogPluginPublisher(CMSPluginBase):
    model = BlogPluginModel  # model where plugin data are saved
    module = 'Blog' #_("Blog")
    name = 'Blog Plugin' #_("Blog Plugin")  # name of the plugin in the interface
    render_template = "blog_cms_integration/blog_plugin.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context
