from cms.toolbar_base import CMSToolbar
from cms.toolbar_pool import toolbar_pool
from cms.utils.urlutils import admin_reverse
from blog.models import Post, Category, Tag


class PostToolbar(CMSToolbar):

    def populate(self):
        """
        Menu for posts
        """
        menu = self.toolbar.get_or_create_menu(
            'blog_cms_integration-posts',  # a unique key for this menu
            'Posts',                        # the text that should appear in the menu
        )
        menu.add_sideframe_item(
            name='Post List',
            url=admin_reverse('blog_post_changelist'),
        )
        # menu.add_modal_item(
        #     name='Add Post',
        #     url=admin_reverse('blog_post_add'),
        # )
        """
        Menu for categories
        """
        menu = self.toolbar.get_or_create_menu(
            'blog_cms_integration-categories',  # a unique key for this menu
            'Category',                        # the text that should appear in the menu
        )
        menu.add_sideframe_item(
            name='Category List',
            url=admin_reverse('blog_category_changelist'),
        )
        menu.add_modal_item(
            name='Add Category',
            url=admin_reverse('blog_category_add'),
        )
        """
        Menu for Tags
        """
        menu = self.toolbar.get_or_create_menu(
            'blog_cms_integration-tags',  # a unique key for this menu
            'Tag',                        # the text that should appear in the menu
        )
        menu.add_sideframe_item(
            name='Tag List',
            url=admin_reverse('blog_tag_changelist'),
        )
        menu.add_modal_item(
            name='Add Tag',
            url=admin_reverse('blog_tag_add'),
        )
        """ 
        Add post button
        """
        # buttonlist = self.toolbar.add_button_list()
        # buttonlist.add_modal_button(
        #     name='Add post',
        #     url=admin_reverse('blog_post_add'),
        # )


# register the toolbar
toolbar_pool.register(PostToolbar)

