from django import forms

from djangocms_text_ckeditor.widgets import TextEditorWidget
from markdownx.fields import MarkdownxFormField

from blog.models import Post


class PostWizardForm(forms.ModelForm):
    # content = forms.CharField(
    #     widget=TextEditorWidget(),
    # )
    # content = MarkdownxFormField()
    class Meta:
        model = Post
        exclude = ['contentHTML']
