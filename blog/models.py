from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

from .managers import CustomUserManager

import markdown


class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['password']

    objects = CustomUserManager()

    def __str__(self):
        return self.email

class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.CharField(max_length=10, unique=True)
    color = models.CharField(max_length=10, blank=True)
    order = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['order', 'name']

    def __str__(self):
        return self.name + " (" + self.slug + ")"

    def get_absolute_url(self):
        return ""
        # return reverse('category-detail', args=[str(self.id)])

class Tag(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.CharField(max_length=10, unique=True)
    color = models.CharField(max_length=10, blank=True)
    order = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['order', 'name']

    def __str__(self):
        return self.name + " (" + self.slug + ")"

    def get_absolute_url(self):
        return ""
        # return reverse('tag-detail', args=[str(self.id)])

class Author(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200, unique=True, null=True, blank=True)
    website = models.CharField(max_length=200, blank=True)
    git = models.CharField(max_length=200, blank=True)
    description = models.CharField(max_length=200, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class Language(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.CharField(max_length=10, unique=True)
    image = models.CharField(max_length=50, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()

    POST_CONTENT_TYPE = (
        (0, 'plain'),
        (1, 'markdown'),
        (2, 'HTML'),
    )
    type = models.IntegerField(choices=POST_CONTENT_TYPE, default=0)
    contentHTML = models.TextField(blank=True, help_text="You can leave empty for auto-convert")
    author = models.ForeignKey(Author, on_delete=models.SET_NULL, null=True, blank=True)
    public = models.BooleanField(default=False)
    pinned = models.BooleanField(default=False)
    pinnedOrder = models.IntegerField(default=0)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    category = models.ManyToManyField(Category)
    tag = models.ManyToManyField(Tag)
    posted_at = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-posted_at', 'title']

    def get_absolute_url(self):
        # return reverse('post-detail', args=[str(self.id)])
        return '/blog/' + str(self.id)

    def __str__(self):
        return self.title

    def convertMarkdown(self):
        extension_configs = {
            'codehilite': {
                # 'linenums': True
            }
        }
        md = markdown.Markdown(extensions=['extra', 'codehilite', 'sane_lists'], extension_configs = extension_configs)
        self.contentHTML = md.convert(self.content)

    def save(self, *args, **kwargs):
        if (self.type == 1):
            self.convertMarkdown()
        else:
            self.contentHTML = self.content

        super(Post, self).save(*args, **kwargs)

class BlogImage(models.Model):
    image = models.FileField(upload_to='blog/')
